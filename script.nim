import strutils, windows, smtp, pop3, net
from times import getTime
from os import existsFile, sleep

proc checkMyPoints():int =
  #Checking points by adding numbers in system file
  var file: File
  while not open(file, "helenRat.txt", fmRead):
    sleep(1000)
    continue
  var line = ""
  while file.readLine(line):
    result += parseInt(line).int
  file.close()

proc creatingLogFile(filename:string)=
  if not existsFile(filename):
    var file = open(filename, fmReadWrite)
    file.close()

proc writingIntoLog(logName: string, points: int)=
  #write into log time and points
  var
    file: File
    text = $(getTime().int) & " " & $points
  while not open(file, logName, fmAppend):
    sleep(1000)
    continue
  let space = 15 - len(text)
  for n in 0..space:
    text.add(" ")
  file.writeLine(text)
  file.close()

proc readingLog(logName: string): seq[int] =
  #reads last line from log file
  var file: File
  while not open(file, logName, fmRead):
    sleep(1000)
    continue
  try:
    let pos = file.getFileSize() - 18
    file.setFilePos(pos)
    let str_seq = file.readLine().split(" ")
    result = @[parseInt(str_seq[0]), parseInt(str_seq[1])]
  except:
    result = @[0, 0]

proc showMessage(header, text: string) =
  #generate message window with arguments and show it
  discard MessageBox(0, text, header, MB_ICONEXCLAMATION or MB_OK)

proc sendMyPointsCore(points:int, sendersMail, password, reciverMail: string) =
  #sending message with points and time to mail via smpt
  let msg_body = $getTime().int & " " & $points
  var msg = createMessage("",
                        msg_body,
                        @[sendersMail])
  var smtpConn = connect("smtp.gmail.com", Port 465, true, true)
  smtpConn.auth(sendersMail, password)
  smtpConn.sendmail(sendersMail, @[reciverMail], $msg)
  smtpConn.close()

proc sendMyPoints(points:int):bool =
  #try/except wrapper for basic async proc
  try:
    sendMyPointsCore(points, "example1@gmail.com", "password", "example2@meta.ua")
    return true
  except:

    return false

proc getFriendPointsCore(userNick, password: string):seq[int] =
  #connect to mail via pop3 and parse body's text from last message
  #newConsoleLogger().addHandler()

  let c = newPOP3Client(host="pop.meta.ua")

  c.user(userNick)
  c.pass(password)
  let mess_list = c.list().body

  let resp = c.retr(msg_num=len(mess_list)).body
  let info = resp[len(resp)-1].split()
  result = @[parseInt(info[0]), parseInt(info[1])]
  c.quit()

proc getFriendPoints(): seq[int] =
  #try/except wrapper for basic proc name(arguments): return type =  
  try:
    result = getFriendPointsCore("login", "password")
  except:
    discard
  return result

proc makeProperEnding(number:int, one, two, five: string):string =
  # takes number and 3 variants of noun and throw correct

  result = $number & " "
  let numEnd = number mod 100

  case numEnd
  of 1, 21, 31, 41, 51, 61, 71, 81, 91:
    result.add(one)
  of 2..4, 22..24, 32..34, 42..44, 52..54, 62..64, 72..74, 82..84, 92..94:
    result.add(two)
  else:
    result.add(five)


proc createMessage(timeDiff, pointsDiff: int, personName: string): string =
  # generate message's text based on time and points difference

  var mess = personName & " "
  case timeDiff
  of 0..400:
    mess.add("займається англійською прямо зараз! ")
  of 401..3600:
    mess.add("займалась англійською ")
    let minutes = (timeDiff/60).int
    mess.add(makeProperEnding(minutes, "хвилину", "хвилини", "хвилин"))
    mess.add(" назад. ")
  else:
    mess.add("займалась англіською більше ")
    let hours = (timeDiff/3600).int
    mess.add(makeProperEnding(hours, "години", "годин", "годин"))
    mess.add(" назад. ")
  mess.add("\n")

  if pointsDiff > 0:
    mess.add("Ти випереджаєш її на " &
     makeProperEnding(pointsDiff, "бал", "бали", "балів") &
      ".\nАле не розслабляйся!)")
  elif pointsDiff < 0:
    mess.add("Ти відстаєш від неї на " &
     makeProperEnding(pointsDiff*(-1), "бал", "бали", "балів") &
      "((\nЧас зібратись з силами і наздогнати її!")
  else:
    mess.add("Нічого собі. Гіп-гоп!!!\nУ вас однакова кількість балів))")
  return mess


proc mainLoop() =
  creatingLogFile("log.txt")
  creatingLogFile("log1.txt")
  var myPoints = checkMyPoints()
  var friendPoints = getFriendPoints()
  var onStart = true
  if friendPoints != nil and readingLog("log1.txt")[1] == friendPoints[1]:
    onStart = false
  var lastMessTime = 0

  while true:
    sleep(120000)

    var myNewPoints = checkMyPoints()

    if myNewPoints > myPoints:
      writingIntoLog("log.txt", myNewPoints)
      discard sendMyPoints(myNewPoints)
      myPoints = myNewPoints

    var friendNewPoints = getFriendPoints()
    var currTime = getTime().int

    if friendPoints != nil and friendNewPoints != nil and 
      (friendNewPoints[1] > friendPoints[1] or onStart) and 
        (lastMessTime == 0 or currTime - lastMessTime > 599):
      let timeDiff = currTime - friendNewPoints[0]
      let pointsDiff =  myPoints - friendNewPoints[1]
      showMessage("English...", createMessage(timeDiff, pointsDiff, "Леся"))
      lastMessTime = currTime
      writingIntoLog("log1.txt", friendNewPoints[1])
      friendPoints = friendNewPoints
    
    onStart = false

mainLoop()
#echo checkMyPoints()
#for n in 0..10:
#  showMessage("HI", makeProperEnding(n, "бал", "бали", "балів"))
#creatingLogFile("log1.txt")
#echo getFriendPoints()
#writingIntoLog(234)
#echo checkMyPoints()
#echo readingLog()
#showMessage("HI", "Helen started to study English")
#echo readingLog()
#discard sendMyPoints(checkMyPoints())
#echo getFriendPoints()
#[
let
  diffss = @[-100, 0, 100]
  timess = @[100, 1000, 10000]
for dd in diffss:
  for tt in timess:
    showMessage("Англійська", createMessage(tt, dd))
  ]#
#echo createMessage(0, 100)
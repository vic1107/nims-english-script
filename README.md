# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is my script for English learning desktop software 'Beginner1' (http://rutracker.org/forum/viewtopic.php?t=2743283). 
The script connects two pupils and shows them notifications about each other progress.

### How do I get set up? ###

This script was written in Nim lang (https://nim-lang.org). It compiled to binnary. 
You need Nim compiler and MinGW compiler installed.
The script was compiled with "--app:gui" argument.